import { DataObjectState } from '@themost/data';
import moment from 'moment';
/**
 * 
 * @param {import('@themost/data').DataEventArgs} event 
 */
async function beforeSaveAsync(event) {
    if (event.state === DataObjectState.Insert) {
        if (event.target.eventHoursSpecification) {
            // it's a recurring event
            // set event instance duration
            event.target.duration = event.target.eventHoursSpecification.duration;
        } else if (event.target.startDate && event.target.endDate) {
            // it's a simple event
            event.target.duration = moment.duration(event.target.endDate.getTime() - event.target.startDate.getTime(), 'milliseconds').toString();
        }
    } else if (event.state === DataObjectState.Update) {
        const duration = await event.model
            .where('id').equal(event.target.id).select('eventHoursSpecification/duration').value();
        if (duration) {
            // it's a recurring event
            // set event instance duration
            event.target.duration = duration;
        } else if (event.target.startDate && event.target.endDate) {
            // it's a simple event
            event.target.duration = moment.duration(event.target.endDate.getTime() - event.target.startDate.getTime(), 'milliseconds').toString();
        }
    }
    
}

function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

export {
    beforeSave
}