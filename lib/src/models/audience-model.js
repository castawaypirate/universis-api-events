import {EdmMapping, DataObject} from '@themost/data';

/**
 * @class
 
 * @property {string} audienceType
 * @augments {DataObject}
 */
@EdmMapping.entityType('Audience')
class Audience extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Audience;
